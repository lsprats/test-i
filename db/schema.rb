# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20210518132634) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "client_temporalies", force: :cascade do |t|
    t.text "id_client"
    t.integer "status", default: 0
    t.index ["id_client"], name: "index_client_temporalies_on_id_client"
  end

  create_table "clients", force: :cascade do |t|
    t.text "id_client"
    t.string "email"
    t.string "first_name"
    t.string "last_name"
    t.string "job"
    t.string "country"
    t.string "address"
    t.string "zip_code"
    t.string "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id_client"], name: "index_clients_on_id_client"
  end

  create_table "discounts", force: :cascade do |t|
    t.text "id_discount"
    t.text "id_client"
    t.text "id_payment"
    t.float "amount"
    t.integer "discount_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id_client"], name: "index_discounts_on_id_client"
    t.index ["id_discount"], name: "index_discounts_on_id_discount"
    t.index ["id_payment"], name: "index_discounts_on_id_payment"
  end

  create_table "payments", force: :cascade do |t|
    t.text "id_payment"
    t.text "id_client"
    t.integer "currency"
    t.float "total_amount"
    t.float "total_discount"
    t.float "total_with_discount"
    t.datetime "payment_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id_client"], name: "index_payments_on_id_client"
    t.index ["id_payment"], name: "index_payments_on_id_payment"
  end

  create_table "transactions", force: :cascade do |t|
    t.text "id_trx"
    t.text "id_client"
    t.text "id_payment"
    t.float "amount"
    t.integer "trx_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id_client"], name: "index_transactions_on_id_client"
    t.index ["id_payment"], name: "index_transactions_on_id_payment"
    t.index ["id_trx"], name: "index_transactions_on_id_trx"
  end

end
