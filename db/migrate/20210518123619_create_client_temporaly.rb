class CreateClientTemporaly < ActiveRecord::Migration[5.1]
  def change
    create_table :client_temporalies do |t|
      t.text :id_client, index: true
      t.integer :status, default: 0
    end
  end
end
