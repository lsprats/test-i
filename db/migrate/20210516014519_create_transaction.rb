class CreateTransaction < ActiveRecord::Migration[5.1]
  def change
    create_table :transactions do |t|
      t.text :id_trx, index: true
      t.text :id_client, index: true
      t.text :id_payment, index: true
      t.float :amount
      t.integer :trx_type
      
      t.timestamps
    end
  end
end
