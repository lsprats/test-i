class CreateDiscount < ActiveRecord::Migration[5.1]
  def change
    create_table :discounts do |t|
      t.text :id_discount, index: true
      t.text :id_client, index: true
      t.text :id_payment, index: true
      t.float :amount
      t.integer :discount_type
      
      t.timestamps
    end
  end
end
