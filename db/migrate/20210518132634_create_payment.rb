class CreatePayment < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.text :id_payment, index: true
      t.text :id_client, index: true
      t.integer :currency
      t.float :total_amount
      t.float :total_discount
      t.float :total_with_discount
      t.datetime :payment_date
      
      t.timestamps
    end
  end
end
