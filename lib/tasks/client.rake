namespace :increase do
  desc "Get Client increase"

  task get_client: :environment do
    
    count=0

    ClientTemporaly.where(status: "ready_to_process").each do |client_temporaly|
      
      result = IncreaseService.get_client(ENV['AUTHORIZATION'], client_temporaly.id_client)

      if result[:status] == 200
        client = result[:response]
        Client.create(id_client: client_temporaly.id_client, email: client["email"], first_name: client["first_name"], last_name: client["last_name"], job: client["job"], country: client["country"], address: client["address"], zip_code: client["zip_code"], phone: client["phone"])
        client_temporaly.update(status: "processed")
        count += 1
      else
        puts "No procesado - id: #{client_temporaly.id_client}"
      end


    end
    
    puts "Clientes insertados: #{count}"

  end
end