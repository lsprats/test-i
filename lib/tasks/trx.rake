Dir[Rails.root.join('app', 'parsers', '*.rb')].each { |f| require f }
Dir[Rails.root.join('app', 'parsers', '**', '*.rb')].each { |f| require f }

namespace :increase do
  desc "Get files trx increase"

  task get_trx: :environment do

    result = IncreaseService.get_txt ENV['AUTHORIZATION']

    if result[:status] == 200
      puts "archivo obtenido correctamente"
      Parsers::TrxClientParser.new.parse(result[:response])
    else
      puts "no descargó el archivo"
    end
  end
end