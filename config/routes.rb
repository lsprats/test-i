Rails.application.routes.draw do
  resources :clients, only: [:index, :show]
  resources :transactions, only: [:index, :show]
  get "balance" => "transactions#balance"
end
