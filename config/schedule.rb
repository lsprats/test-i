# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
set :output, "#{path}/log/file.log"
#
every 1.minutes do
	puts "Get TRX"
    rake "increase:get_trx"
end
#
every 1.minutes do
	puts "Get Client"
    rake "increase:get_client"
end