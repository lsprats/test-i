# Documentacion - Increase

API: La api consta de tres endpoints, para consultar toda la informacion de acuerdo al planteo del test

1- Clientes
  - Devuelve todos los clientes que se encuentran en la base de datos
  - Devuelve un cliente en especial de acuerdo a su id
2- Transacciones
  - Devuelve todas las transacciones de un cliente en especifico de acuerdo a su id_client
3- Balance
  - Devuelve todo lo cobrado de un cliente mediante su id_client


Nota: Se adjunta un postman collection para pruebas



En cuanto al planteo general, se hizo de la siguiente manera

- Para consumir los datos de los endpoints, se creó dos rakes, los cuales se ejecutarán cada 1 minuto mediante un whenever (cron).

  - trx.rake: Obtiene el file.txt mediante un service.
  Si todo esta correcto, ejecuta el parser (TrxClientParser) que basicamente, realiza el parseo linea por linea mediante una estructura del file previamente definida.

  De acuerdo al tipo de registro, se guarda la informacion en cada modelo. Previo a consulta para evitar duplicar data. 

  En el caso del Cliente, debido a la inestabilidad del endpoint, decidí crear una tabla temporal ClientTemporaly, para guardar los id_client. Previo a consulta para evitar duplicar data.

  - client.rake: Recorre cada registro del modelo ClientTemporaly en estado "ready_to_process", ejecuta el service que recupera los datos del cliente y los guarda en el modelo Cliente. Posterior a esto, actualizamos el status del registro a "processed", para que el rake no vuelva a procesarlo.


  De esta manera, ejecutando los rakes en ese lapso de tiempo, evitamos perdida de datos por expiracion de caché o inestabilidad del servicio de transacciones.


# Posibles mejoras - Increase

  - Crear test's
  - Agregar mail de alertas cuando el servicio de transacciones esté caido.
  - Agregar paginación en clientes y transacciones
  - Mejorar las relaciones entre modelos
  - Manejo de errores