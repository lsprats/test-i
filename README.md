# README

* Ruby: 2.7.1
* Rails version: 5.1
* DB: Postgres

- Pasos para ejecutar el proyecto Increase.

* Configuración:

1.-Clonar el proyecto
  * git clone https://github.com/lsprats/increase.git

2.-Dentro del proyecto Increase/ ejecutar los siguientes comandos
  
  * bundle install
  * Modificar en database.yml los datos de conexion con pg
  * rake db:create && rake db:migrate
  * En environment_variables.rb se encuentra el ENV['AUTHORIZATION']


3.-Ejecutar cron
  
  * whenever --update-crontab --set environment='development' crontab -r
    Este comando ejecuta los rakes automaticamente cada 1minutos (increase:get_trx, rake increase:get_client)


4.-Importar el Collection de Postman, para poder usar los endpoints desarrollados.


# OTROS

* En caso de no tener instalado Postgress (Ubuntu)

1- Instalar postgres

   * sudo apt-get install postgresql libpq-dev phppgadmin pgadmin4

2- Configurando postgre

   * sudo nano /etc/postgresql/9.1/main/postgresql.conf
    -Descomentar #listen_addresses = localhost
    -Descomentar #password_encryption = on

Guardar archivo y reiniciar

  * sudo service postgresql restart