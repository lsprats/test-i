module Parsers

  class TrxClientParser < BaseParser
    attr_reader :report_client

    POSITION = 0
    REG_TYPE = {
      '1' => 'header',
      '2' => 'trx',
      '3' => 'discount',
      '4' => 'footer'
    }.freeze


    def initialize
      super('trx_client')
    end

    def reg_type(line)
      REG_TYPE[line[POSITION]]
    end

    def parse(document)
      document.each_line.lazy.each do |line|
        encoded_line = line.encode('iso-8859-1', invalid: :replace, undef: :replace)
        register(encoded_line)
      end
    end

    def register(line)
      register = read_line(line)
      transaction(register, line)
    end

    private

    def transaction(register, line)
      case reg_type(line)
      when 'header'
        header(register)
      when 'trx'
        trx(register)
      when 'discount'
        discount(register)
      when 'footer'
        footer(register)
      else
        raise StandardError('registro desconocido')
      end
    end

    def header(register)
      @transactions_data = []
      @discounts_data = []

      @id_pago             = register["id_pago"]
      @moneda              = register["moneda"]
      @monto_total         = register["monto_total"]
      @total_descuento     = register["total_descuentos"]
      @total_con_descuento = register["total_con_descuentos"]
    end

    def trx(register)
      trx = Transaction.where(id_trx: register["id_transaccion"])

      unless trx.count > 0
        @transactions_data << Hash[id_trx: register["id_transaccion"], id_payment: @id_pago, amount: register["monto"], trx_type: register["tipo"].to_i]
      end

    end

    def discount(register)
      discount = Discount.where(id_discount: register["id_descuento"])

      unless discount.count > 0
        @discounts_data << Hash[id_discount: register["id_descuento"], id_payment: @id_pago, amount: register["monto"], discount_type: register["tipo"].to_i]
      end

    end

    def footer(register)
      @id_descuento  = register["id_descuento"]
      @fecha_de_pago = register["fecha_de_pago"]
      @id_cliente    = register["id_cliente"]
      @currency = @moneda == '000' ? 0  : 1

      payment = Payment.where(id_payment: @id_pago)
      unless payment.count > 0
        Payment.create(id_payment: @id_pago, id_client: @id_cliente, currency: @currency, total_amount: @monto_total.to_f, total_discount: @total_descuento.to_f, total_with_discount: @total_con_descuento.to_f, payment_date: @fecha_de_pago)
      end

      Transaction.create(@transactions_data)
      Discount.create(@discounts_data)

      Transaction.where(id_payment: @id_pago).update(id_client: @id_cliente)
      Discount.where(id_payment: @id_pago).update(id_client: @id_cliente)

      client = Client.where(id_client: @id_cliente)

      unless client.count > 0
        client_temporaly = ClientTemporaly.where(id_client: @id_cliente)
        unless client_temporaly.count > 0
          puts "No encotró el Cliente--- #{@id_cliente}"
          ClientTemporaly.create(id_client: @id_cliente)
        end
      end
    end

  end
end
