class BaseParser
  attr_reader :records_structure

  def initialize(parser_folder)
    @records_structure = YAML.load_file(Rails.root.join('app', 'parsers', parser_folder, 'structure.yml'))
  end

  def read_line(line)
    record_fields = records_structure[reg_type(line)].keys
    record_format = records_structure[reg_type(line)].values.map { |e| 'A' + e.to_s }.join

    Hash[record_fields.zip(line.unpack(record_format))]
  end
end
