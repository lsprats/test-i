class ClientsController < ApplicationController

  def index
    clients = Client.all
    render json: { data: clients }, status: :ok
  end

  def show
    client = Client.find(params[:id])
    render json: { data: client }, status: :ok
  end

  private

  def client_params
    params.permit(:id)
  end

end