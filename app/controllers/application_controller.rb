class ApplicationController < ActionController::Base
  
  before_action :authorized


  def authorized
    render json: { message: 'Please send Authorization' }, status: :unauthorized unless request.headers["Authorization"].present?
    @authorization = request.headers["Authorization"]
  end  

end
