class TransactionsController < ApplicationController

  def index
    transactions = Transaction.where(id_client: params["id_client"])
    render json: { data: transactions }, status: :ok
  end

  def show
    transaction = Transaction.find(params[:id])
    render json: { data: transaction }, status: :ok
  end

  def balance
    if params[:id_client].nil?
      render json: {status: 400, message: "El parametro id_client es requerido"}, status: :bad_request
    else
      @payment = Payment.where(id_client: params[:id_client]).first
    end
    render json: { "Id_client": params[:id_client], "Total Amount": @payment.total_amount, "Amount Discount": @payment.total_discount, "Amount with Discount": @payment.total_with_discount }   
  end

  private

  def transaction_params
    params.permit(:id, :id_client)
  end

end