class IncreaseService
  
  class << self
    include RestClient

    def get_txt(x_authorization)
      headers = get_headers(x_authorization)
      begin
        response = RestClient.get("https://increase-transactions.herokuapp.com/file.txt", headers)
      rescue RestClient::ExceptionWithResponse => e
        return { response: {message: "error_#{e}" }, status: 400 }
      end
      return {response: response, status: 200}
    end

    def get_client(x_authorization, id_client)
      headers = get_headers(x_authorization)
      begin
        response = JSON.parse(RestClient.get("https://increase-transactions.herokuapp.com/clients/" + id_client, headers))
      rescue RestClient::ExceptionWithResponse => e
        return { response: {message: "error_#{e}" }, status: 400 }
      end
      return {response: response, status: 200}
    end
    
    private
    
    def get_headers(x_authorization)
      headers = {
        'accept': 'application/json',
        'content-type': 'application/json',
        'Authorization': x_authorization,
      }
      return headers
    end
            
  end
  
end
