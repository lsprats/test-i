class Payment < ApplicationRecord
  validates  :id_payment, presence: true
  enum currency: %i[ars usd]
end